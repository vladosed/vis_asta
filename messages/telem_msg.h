#include "../common_includes.h"

typedef struct {
  uint8_t PresetNumber;
  float vbat;//               Float;
  uint16_t state;//              word;
  uint32_t flags;//              Longword;
  float ABSOLUTE_HEADING; //   Float;
  float ALTITUDE; //           Float;
  float ALTITUDE_HOLD_SPEED; //Float;
  double gps_lat; //            double;
  double gps_lon; //            double;
  float GPS_HACC; //           Float;//точность определения
  float gps_hdop; //           Float;
  uint8_t GPS_SATELLITES_FIX; //   byte;
  uint8_t GPS_NUMBER_OF_SATELLITES; // byte;//93
  float RTK_DIFF_AGE; //       Float;//244
  float GIMBAL_ANGLE_PITCH; // Float;//255
  float GYRO_TOTAL_SPEED; //   Float;//365
} asta_telemetry_msg;
/*
int telemetry_parse(){
    
}*/
/*
void telem_packet_make(uint16_t cmd, void *body, uint16_t bodylen)
{
    sCoptHdr *Hdr = (sCoptHdr*) OutBuff;
    Hdr->magic0 = 0x41;
    Hdr->magic1 = 0x43;
    Hdr->magic2 = 0x35;
    Hdr->packetLen = bodylen+16;//размер полезной нагрузки плюс заголовок кроме 3 байт идентификатора
    Hdr->packetNumber = Hdr->packetNumber+1;
    Hdr->senderID = GCS_ID;//id наземки
    //Hdr->recipientID = (0x0FFF);//id коптера
    Hdr->recipientID = (0x0017);//id коптера
    Hdr->packetTime = (0);//мсек актуальные
    Hdr->commandNumber = (Hdr->commandNumber)+1;//номер команды
    Hdr->commandType = (cmd);//тип команды 19
    if (bodylen)//если полезная нагрузка не пустая то присоединяю полезную нагрузку
        memcpy(&(OutBuff[sizeof(sCoptHdr)]),body,bodylen);
    OutSize = sizeof(sCoptHdr)+bodylen+2;//длинна всего пакета плюс 2 байта контрольной суммы
    PacketCRCRecount();//контрольная сумма
}*/