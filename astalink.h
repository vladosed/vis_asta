#pragma once
#include "structs.h"

#define CMD_CONTROL_MAX_PACKET_SIZE 512
#define GCS_ID 0x4001

static uint8_t inner_buffer[CMD_CONTROL_MAX_PACKET_SIZE];
static uint8_t inner_CRC_a;
static uint8_t inner_CRC_b;

static uint8_t out_buffer[CMD_CONTROL_MAX_PACKET_SIZE];
static uint16_t out_buffer_size;

static int m_pack_size = 0;


static uint16_t get_message_type(BortHdr_t *header, void *body){
    return header->commandType;
}

static void packetCRCRecount()
{
    uint8_t CRC_A = 0;
    uint8_t CRC_B = 0;
    for (int i = 3; i<(out_buffer_size-2); i++) {
        CRC_A += out_buffer[i];
        CRC_B += CRC_A;
    }
    out_buffer[out_buffer_size-2] = CRC_A;
    out_buffer[out_buffer_size-1] = CRC_B;
}

static uint16_t packetMake(uint16_t cmd, uint16_t id_cop, void *body, uint16_t bodylen, uint8_t * out)
{
    sCoptHdr *Hdr = (sCoptHdr*)out_buffer;
    Hdr->magic0 = 0x41;
    Hdr->magic1 = 0x43;
    Hdr->magic2 = 0x35;
    Hdr->packetLen = bodylen+16;//размер полезной нагрузки плюс заголовок кроме 3 байт идентификатора
    Hdr->packetNumber = 0x0001;
    Hdr->senderID = GCS_ID;//id наземки
    //Hdr->recipientID = (0x0FFF);//id коптера
    Hdr->recipientID = 0x0FFF;//id коптера
    Hdr->packetTime = (0);//мсек актуальные
    Hdr->commandNumber = (Hdr->commandNumber)+1;//номер команды
    Hdr->commandType = (cmd); //тип команды 19
    if (bodylen) //если полезная нагрузка не пустая то присоединяю полезную нагрузку
        memcpy(&(out_buffer[sizeof(sCoptHdr)]),body,bodylen);
    out_buffer_size = sizeof(sCoptHdr)+bodylen+2;//длинна всего пакета плюс 2 байта контрольной суммы
    packetCRCRecount(); //контрольная сумма
    memcpy(out, out_buffer, out_buffer_size);
    return out_buffer_size;
}



static uint16_t astalink_parse_char(int chan, uint8_t byte, uint8_t * message, int * len)
{
    uint16_t PackLen_;
    switch (m_pack_size) {
    case 0:
        if (byte == 0x41) {//читаю первый байт 'A'
            inner_buffer[m_pack_size++] = byte;
            inner_CRC_a = 0;
            inner_CRC_b = 0;
        };
        break;
    case 1:
        if (byte == 0x43) {//читаю второй байт 'C'
            inner_buffer[m_pack_size++] = byte;
        }
        else m_pack_size = 0;
        break;
    case 2:
        if (byte == 0x35) {//читаю третий байт '5'
            inner_buffer[m_pack_size++] = byte;
        }
        else m_pack_size = 0;
        break;
    case 3:
        inner_buffer[m_pack_size++] = byte;//количество байт пакета - начало
        inner_CRC_a += byte;//тут начинаю считать контрольную сумму
        inner_CRC_b += inner_CRC_a;
        break;
    case 4:
        inner_buffer[m_pack_size++] = byte;//количество байт пакета - конец
        inner_CRC_a += byte;
        inner_CRC_b += inner_CRC_a;
        PackLen_= (uint16_t) inner_buffer[3];    //количество байт пакета из буфера - начало
        //return PackLen_;
        //PackLen_+=inner_buffer[3]<<8;//количество байт пакета из буфера - конец

        //PackLen_ = e_uint16(((sCoptHdr*)inner_buffer)->packetLen);
        if ((PackLen_<16) || (PackLen_>(CMD_CONTROL_MAX_PACKET_SIZE-16)))
            m_pack_size = 0;//если размер будущего пакета не корректный то прекращаю читать
        break;
    default:
        inner_buffer[m_pack_size++] = byte;
        PackLen_= (uint16_t) inner_buffer[3]; // здесь может быть ошибка
        //PackLen_ = e_uint16(((sCoptHdr*)inner_buffer)->packetLen);//количество байт пакета - конец
        if (m_pack_size < (PackLen_ + 4)) {
            inner_CRC_a += byte;
            inner_CRC_b += inner_CRC_a;
        }
        else {
            if (m_pack_size >= (PackLen_ + 5)) {
                // Packet received
                if ((inner_buffer[m_pack_size-2]==inner_CRC_a) && (inner_buffer[m_pack_size-1]==inner_CRC_b)) {
                    //qDebug() << "message recived";
                    *len = m_pack_size;
                    memcpy(message, inner_buffer, m_pack_size);
                    m_pack_size = 0;
                    return 1;//get_message_type((BortHdr_t*) inner_buffer, &(inner_buffer[19]));
                    //return m_pack_size;
                    //PacketReceived((sCoptHdr*)inner_buffer, &(inner_buffer[19]));
                }
                else {
                        
                 m_pack_size = 0;
                 //MDF_LOGD("LOL");
                }                
            }
        }
        break;
    };

    return 0;
}